﻿using MovieApp.Models;
using MovieApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieApp.Controllers
{
    public class CustomersController : Controller
    {
        private MovieAppContext _context;

        public CustomersController()
        {
            _context = new MovieAppContext();
        }
        // GET: Customers
        public ActionResult RandomCustomer()
        {
            var customer = new List<Customer> {
                new Customer {Name = "customer1", Id = 1},
                new Customer {Name = "Secondcustomer", Id = 2 },
                new Customer {Name = "thirdcustomer", Id = 3},
                new Customer {Name = "fourthcustomer", Id = 4}
            };
            var viewModel = new RandomMovieViewModel {
                Customers = customer
            };
            return View(viewModel);
        }

        [Route("customers/details/{id}")]
        public ActionResult CustomerDetails(int id)
        {
            var customer = new List<Customer>();
            var customerName = string.Empty;

            foreach (var item in customer)
            {
                if (item.Id == id)
                {
                    customerName = item.Name;
                }
                break;
            }
            return Content(string.Format("id: {0}", id));
        }
    }
}