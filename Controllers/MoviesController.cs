﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieApp.Models;
using MovieApp.ViewModels;

namespace MovieApp.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies
        public ActionResult Random()
        {
            var movie = new List<Movie> {
                new Movie {Name = "Sherlock Holmes", Id = 1},
                new Movie {Name = "Avengers", Id = 2},
                new Movie {Name = "Wakanda", Id = 3},
                new Movie {Name = "Shek", Id = 4},
                new Movie {Name = "Family Guy", Id = 5}
            };

            var customer = new List<Customer> {
                new Customer {Name = "customer1", Id = 1},
                new Customer {Name = "Secondcustomer", Id = 2 },
                new Customer {Name = "thirdcustomer", Id = 3},
                new Customer {Name = "fourthcustomer", Id = 4}
            };

            //this view here will basically carry all of our data above (movie object, customer)
            var viewModel = new RandomMovieViewModel
            {
                Movie = movie,
                Customers = customer
            };

            return View(viewModel);
        }
        public ActionResult Edit(int id)
        {
            return Content("Returned id: " + id);
        }


        //to make a parameter optional we make it nullable by adding a question mark 
        public ActionResult Index(int? pageIndex, string sortBy)
        {
            if (!pageIndex.HasValue)
            {
                pageIndex = 1;
            }
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "Name";
            }

            return Content(string.Format("Page index: {0}, SortBy: {1}", pageIndex, sortBy));
        }

        [Route("movies/released/{year}/{month:regex(\\d{2}):range(1,12)}")]
        public ActionResult ByReleaseDate(int year, int month)
        {
            return Content(string.Format("Release Date: {0},{1}", year, month));
        }
    }
}